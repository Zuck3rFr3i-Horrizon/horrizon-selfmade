--//
	-- Author: Zuck3rFr3i
--\\
sx, sy = guiGetScreenSize
player = getLocalPlayer
strPname = getPlayerName(player)
strPserial = getPlayerSerial(player)
isPlayerLoggedin = false

--//
	-- Disable some Standart Gui
--\\
addEventHandler("onClientResourceStart", getResourceRootElement(), function()
	guiSetInputMode("no_binds_when_editing")
	showPlayerHudComponent("ammo", false)
	showPlayerHudComponent("armour", false)
	showPlayerHudComponent("clock", false)
	showPlayerHudComponent("health", false)
	showPlayerHudComponent("money", false)
	showPlayerHudComponent("weapon", false)
	showPlayerHudComponent("wanted", false)
end)

--//
	-- Disable Ped Damage if it was set in Database
--\\
aeh("onClientPedDamage", getRootElement(), function()
    if getElementData(source, "god") == 1 then
        cancelEvent()
    end
end)